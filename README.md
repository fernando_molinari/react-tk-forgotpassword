# react-tk-forgotpassword

> reusable forgotpassword package for tk

[![NPM](https://img.shields.io/npm/v/react-tk-forgotpassword.svg)](https://www.npmjs.com/package/react-tk-forgotpassword) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-tk-forgotpassword
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-tk-forgotpassword'
import 'react-tk-forgotpassword/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [fernando_molinari](https://github.com/fernando_molinari)
