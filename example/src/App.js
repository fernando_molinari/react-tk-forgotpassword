import React from "react";
import ForgotPasswordContent from "react-tk-forgotpassword";
import "react-tk-forgotpassword/dist/index.css";

const App = () => {
  const [fpBtnEnabled, setFpBtnEnabled] = React.useState(false);
  const onForgotPasswordClick = (payload) => {
    setFpBtnEnabled(!fpBtnEnabled);
  };

  return (
    <div className="App">
      <ForgotPasswordContent
        onForgotPasswordClick={onForgotPasswordClick}
        schoolName=""
        forgotPasswordBtnEnabled={fpBtnEnabled}
        headerText="Thalamus Knowledge"
      />
    </div>
  );
};

export default App;
