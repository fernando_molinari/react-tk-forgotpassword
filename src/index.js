/*
 * Copyright 2021 by - - Fernando Molinari.
 * All rights reserved.
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

const ForgotPasswordContent = (props) => {
  const { schoolName, forgotPasswordBtnEnabled, headerText, t } = props;

  const useStyles = makeStyles((theme) => ({
    paper: {
      margin: headerText ? theme.spacing(2, 4) : theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    btnWrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
      width: '40%',
    },
    paragraph: {
      fontSize: '0.8rem',
      marginTop: '20px',
    },
    headerText: {
      marginTop: '0',
      marginBottom: '10px',
    },
  }));

  const ValidateEmail = (email) => {
    if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      return true;
    }
    return false;
  };

  const classes = useStyles();

  let sName = '';

  if (schoolName) {
    sName = `/${schoolName}`;
  }

  const [btnDisabled, setBtnDisabled] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);

  const [userName, setUserName] = useState(null);
  const [isUserNameError, setIsUserNameError] = useState(false);
  const [isUserNameHelperText, setIsUserNameHelperText] = useState('');

  const onHandleUserName = (e) => {
    setIsUserNameError(false);
    setIsUserNameHelperText('');
    setUserName(e.target.value.trim());
  };

  useEffect(() => {
    setBtnDisabled(false);
    setBtnLoading(false);
  }, [forgotPasswordBtnEnabled]);

  const onForgotPasswordClick = (event) => {
    event.preventDefault();

    setIsUserNameError(false);

    if (!userName) {
      setIsUserNameError(true);
      setIsUserNameHelperText(
        t ? t('plsUserName') : 'Please type in your User Name',
      );
      return false;
    }

    // email wrong format
    if (!ValidateEmail(userName)) {
      setIsUserNameError(true);
      setIsUserNameHelperText(
        t ? t('plsEmail') : 'Please type in a valid email',
      );
      return false;
    }

    const payload = {
      userName,
    };

    props.onForgotPasswordClick(payload);

    setBtnDisabled(true);
    setBtnLoading(true);
  };

  return (
    <div className={classes.paper}>
      {headerText ? (
        <Typography component='h1' variant='h6' className={classes.headerText}>
          {headerText}
        </Typography>
      ) : null}
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component='h1' variant='h6'>
        {t ? t('forgotResetPassword') : 'Forgot/Reset Password'}
      </Typography>
      <Typography component='h6' className={classes.paragraph}>
        {t
          ? t('forgotPasswordHeader')
          : 'Please enter the email address you used to create your account and we will send you a reset link.'}
      </Typography>
      <form className={classes.form} noValidate>
        <TextField
          variant='outlined'
          margin='normal'
          required
          error={isUserNameError}
          helperText={isUserNameHelperText}
          fullWidth
          id='userName'
          label={t ? t('userNameEmail') : 'Username (email)'}
          name='userName'
          autoComplete='userName'
          onChange={onHandleUserName}
          type='email'
          autoFocus
        />
        <Grid className={classes.btnWrapper}>
          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}
            onClick={onForgotPasswordClick}
            disabled={btnDisabled}
            startIcon={
              btnLoading ? (
                <CircularProgress size='1.0rem' color='primary' />
              ) : null
            }
          >
            {t ? t('sendResetPassword') : 'Send Link'}
          </Button>
        </Grid>
        <Grid container>
          <Grid item xs>
            <Link href={`${sName}/signin`} variant='body2'>
              {t ? t('backSignin') : 'Back to Sign In'}
            </Link>
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

export default ForgotPasswordContent;

ForgotPasswordContent.propTypes = {
  setLoading: PropTypes.func,
  schoolName: PropTypes.string,
  forgotPasswordBtnEnabled: PropTypes.bool,
  onForgotPasswordClick: PropTypes.func,
  t: PropTypes.func,
};
